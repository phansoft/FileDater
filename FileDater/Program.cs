﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace FileDater
{
    class Program
    {
        static void Main(string[] args)
        {
            var fr = new FilesReader();
            var configLines = File.ReadAllLines(args[0]);
            foreach (var cl in configLines)
            {
                if (string.IsNullOrWhiteSpace(cl)) continue;
                if (cl.StartsWith("#")) continue;//comment
                var parts = cl.Trim().Split(" ".ToCharArray(), 3);
                switch (parts[0])
                {
                    case "FORMAT":
                        fr.Format = parts[1];
                        break;
                    case "FOLDER":
                        fr.Folder = parts[1];
                        break;
                    case "FILTER":
                        fr.Filter();
                        break;
                    case "ONLY":
                        fr.Only(int.Parse(parts[1]));
                        break;
                    case "SKIP":
                        fr.Skip(int.Parse(parts[1]));
                        break;
                    case "INVERT":
                        fr.Invert();
                        break;
                    case "RESET":
                        fr.Reset();
                        break;
                    case "DELETE":
                        fr.Delete();
                        break;
                    case "SHOW":
                        var lines = fr.Show(cl.Substring(4).Trim());
                        foreach (var line in lines)
                        {
                            Console.WriteLine(line);
                        }
                        break;
                    case "COPY":
                        if (parts.Length > 2)
                        {
                            fr.Copy(parts[1], parts[2]); //copy with rename
                        }
                        else
                        {
                            fr.Copy(parts[1]);
                        }
                        break;
                    case "MOVE":
                        if (parts.Length > 2)
                        {
                            fr.Move(parts[1], parts[2]); // move with rename
                        }
                        else
                        {
                            fr.Move(parts[1]);
                        }
                        break;
                    default:
                        fr.Filters.Add(cl);
                        break;
                }
            }
        }
    }


    class FilesReader
    {
        private string _format;
        public string Format
        {
            get { return _format; }
            set
            {
                _format = value;
                if (!string.IsNullOrWhiteSpace(Format) && !string.IsNullOrWhiteSpace(Folder))
                {
                    LoadFiles();
                }
            }
        }
        private string _folder;
        public string Folder
        {
            get { return _folder; }
            set
            {
                _folder = value;
                if (!string.IsNullOrWhiteSpace(Format) && !string.IsNullOrWhiteSpace(Folder))
                {
                    LoadFiles();
                }
            }
        }

        private readonly List<DatedFile> _files = new List<DatedFile>();
        public List<String> Filters = new List<string>();
        protected void LoadFiles()
        {
            _files.Clear();
            if (string.IsNullOrWhiteSpace(Format) || string.IsNullOrWhiteSpace(Folder))
            {
                throw new Exception("You must specify a format and a folder before loading files");
            }
            foreach (var file in Directory.GetFiles(Folder))
            {
                DateTime dt;

                if (DateTime.TryParseExact(Path.GetFileName(file), Format, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dt))
                {
                    var df = new DatedFile { OriginalFilename = file, FileDateTime = dt };
                    _files.Add(df);
                }
            }
        }

        public void Invert()
        {
            foreach (var file in _files)
            {
                file.Included = !file.Included;
            }
        }


        public void Reset()
        {
            foreach (var file in _files)
            {
                file.Included = true;
            }
        }

        public void Filter()
        {
            foreach (var file in _files)
            {
                file.Filter(Filters);
            }
        }


        /// <summary>
        /// Skips a subset of the results
        /// for SKIP 10 will remove every tenth file
        /// </summary>
        /// <param name="factor"></param>
        public void Skip(int factor)
        {
            int count = 0;
            foreach (var file in _files.Where(f => f.Included).OrderBy(f => f.FileDateTime))
            {
                count++;
                if (count % factor == 0)
                {
                    file.Included = false;
                }
            }
        }

        /// <summary>
        /// Takes only a subset of the results
        /// for ONLY 10 will take only every tenth file
        /// </summary>
        /// <param name="factor"></param>
        public void Only(int factor)
        {
            int count = 0;
            foreach (var file in _files.Where(f => f.Included).OrderBy(f => f.FileDateTime))
            {
                count++;
                if (count % factor != 0)
                {
                    file.Included = false;
                }
            }
        }

        public List<string> Show(string showFormat = "")
        {
            List<string> ret = new List<string>();
            int count = 0;

            var options = showFormat.Split(' ');
            var total = _files.Count(f => f.Included);
            var countFormat = string.Empty;
            for (var i = 0; i < total.ToString(CultureInfo.InvariantCulture).Length; i++)
            {
                countFormat += "0";
            }
            foreach (var file in _files.Where(f => f.Included).OrderBy(f => f.FileDateTime))
            {
                count++;
                if (string.IsNullOrWhiteSpace(showFormat))
                {
                    ret.Add(file.NewFilename);
                }
                else
                {
                    var line = string.Empty;
                    foreach (var option in options)
                    {
                        switch (option)
                        {
                            case "Count":
                                line += " " + count.ToString(countFormat);
                                break;
                            case "OriginalFilename":
                                line += " " + file.OriginalFilename;
                                break;
                            case "NewFilename":
                                line += " " + file.NewFilename;
                                break;
                            default:
                                if (option.StartsWith("%"))
                                {
                                    line += " " + file.FileDateTime.ToString(option.Substring(1));
                                }
                                else
                                {
                                    line += " " + option;
                                }
                                break;
                        }
                    }
                    ret.Add(line.Trim());
                }
            }
            return ret;
        }


        private void UpdateNewfile(string newDirectory, string showFormat = "")
        {
            var count = 0;
            var options = showFormat.Split(' ');
            var total = _files.Count(f => f.Included);
            var countFormat = string.Empty;
            for (var i = 0; i < total.ToString(CultureInfo.InvariantCulture).Length; i++)
            {
                countFormat += "0";
            }
            foreach (var file in _files.Where(f => f.Included).OrderBy(f => f.FileDateTime))
            {
                count++;
                if (string.IsNullOrWhiteSpace(showFormat))
                {
                    file.NewFilename = newDirectory + Path.GetFileName(file.OriginalFilename);

                }
                else
                {
                    file.NewFilename = newDirectory;
                    foreach (var option in options)
                    {
                        switch (option)
                        {
                            case "Count":
                                file.NewFilename += count.ToString(countFormat);
                                break;
                            case "OriginalFilename":
                                file.NewFilename += Path.GetFileNameWithoutExtension(file.OriginalFilename);
                                break;
                            default:
                                if (option.StartsWith("%"))
                                {
                                    file.NewFilename += file.FileDateTime.ToString(option.Substring(1));
                                }
                                else
                                {
                                    file.NewFilename += option;
                                }
                                break;
                        }
                    }
                    file.NewFilename += Path.GetExtension(file.OriginalFilename);
                }
            }
        }


        public void Copy(string newDirectory, string showFormat = "")
        {
            UpdateNewfile(newDirectory, showFormat);
            foreach (var file in _files.Where(f => f.Included))
            {
                File.Copy(file.OriginalFilename, file.NewFilename, true);
            }
        }

        public void Move(string newDirectory, string showFormat = "")
        {
            UpdateNewfile(newDirectory, showFormat);
            foreach (var file in _files.Where(file => file.Included && file.OriginalFilename != file.NewFilename))
            {
                if (File.Exists(file.NewFilename))
                {
                    File.Delete(file.NewFilename);
                }
                File.Move(file.OriginalFilename, file.NewFilename);
            }
        }


        public void Delete()
        {
            foreach (var file in _files.Where(file => file.Included))
            {
                if (File.Exists(file.OriginalFilename))
                {
                    File.Delete(file.OriginalFilename);
                }
            }
        }
    }

    class DatedFile : IComparable<DatedFile>
    {
        private string _originalFilename;
        public string OriginalFilename
        {
            get { return _originalFilename; }
            set
            {
                _originalFilename = value;
                NewFilename = value;
            }
        }

        public DatedFile()
        {
            Included = true;
        }

        public string NewFilename { get; set; }

        public DateTime FileDateTime { get; set; }
        public bool Included { get; set; }

        public void Filter(List<String> filters)
        {
            //Included = true;
            foreach (var filter in filters)
            {
                var commands = filter.Split(" ".ToCharArray(), 3);
                switch (commands[0])
                {
                    case "AFTER":
                        Included = Included && After(DateTime.Parse(commands[1], CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault));
                        break;
                    case "BEFORE":
                        Included = Included && Before(DateTime.Parse(commands[1], CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault));
                        break;
                    case "INCLUDE":
                        Included = Included && Include(commands[1], commands[2]);
                        break;
                    case "EXCLUDE":
                        Included = Included && Exclude(commands[1], commands[2]);
                        break;
                    case "NEWER":
                        Included = Included && Newer(TimeSpan.Parse(commands[1]));
                        break;
                    case "OLDER":
                        Included = Included && Older(TimeSpan.Parse(commands[1]));
                        break;
                }
            }
        }



        private bool After(DateTime dt)
        {
            return dt.Year == 1 ? FileDateTime.TimeOfDay > dt.TimeOfDay : FileDateTime > dt;
        }

        private bool Before(DateTime dt)
        {
            return dt.Year == 1 ? FileDateTime.TimeOfDay < dt.TimeOfDay : FileDateTime < dt;
        }

        private bool Include(string left, string right)
        {
            return FileDateTime.ToString(left) == right;
        }

        private bool Exclude(string left, string right)
        {
            return !Include(left, right);
        }

        private bool Newer(TimeSpan dt)
        {
            return (FileDateTime.Add(dt) > DateTime.Now);
        }

        private bool Older(TimeSpan dt)
        {
            return (FileDateTime.Add(dt) <= DateTime.Now);
        }


        #region Implementation of IComparable<in DatedFile>

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other"/> parameter.Zero This object is equal to <paramref name="other"/>. Greater than zero This object is greater than <paramref name="other"/>. 
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public int CompareTo(DatedFile other)
        {
            return FileDateTime.CompareTo(other.FileDateTime);
        }

        #endregion
    }

}
